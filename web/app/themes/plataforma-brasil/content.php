<?php
/**
 * @package plataforma-brasil
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
  <div class="row">
  <div class="col-md-6">
		<?php the_title( sprintf( '<h1 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h1>' ); ?>
  </div>
  <div class="col-md-1 visible-md-block visible-lg-block">
    <div class="separator"></div>
  </div>
  <div class="col-md-5">
		<?php if ( 'post' == get_post_type() ) : ?>
		<div class="entry-meta">
      <div>Por <?php the_author(); ?></div>
      <div><?php the_date('d/m/Y'); ?></div>
		</div><!-- .entry-meta -->
		<?php endif; ?>
  </div>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
			/* translators: %s: Name of current post */
			the_content( sprintf(
				__( '<div class="pb-button">Leia mais</div>', 'plataforma-brasil' ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );
		?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'plataforma-brasil' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php plataforma_brasil_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
