<?php
/**
 * The template for displaying comments.
 *
 * The area of the page that contains both current comments
 * and the comment form.
 *
 * @package plataforma-brasil
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */

function time2str($ts) {
    if(!ctype_digit($ts)) {
        $ts = strtotime($ts);
    }
    $diff = time() - $ts;
    if($diff == 0) {
        return 'agora';
    } elseif($diff > 0) {
        $day_diff = floor($diff / 86400);
        if($day_diff == 0) {
            if($diff < 60) return 'agora mesmo';
            if($diff < 120) return '1 minuto atrás';
            if($diff < 3600) return floor($diff / 60) . ' minutos atrás';
            if($diff < 7200) return '1 hour ago';
            if($diff < 86400) return floor($diff / 3600) . ' horas atrás';
        }
        if($day_diff == 1) { return 'Ontem'; }
        if($day_diff < 7) { return $day_diff . ' dias atrás'; }
        if($day_diff < 31) { return ceil($day_diff / 7) . ' semanas atrás'; }
        if($day_diff < 60) { return 'mês passado'; }
        return date('F Y', $ts);
    } else {
        $diff = abs($diff);
        $day_diff = floor($diff / 86400);
        if($day_diff == 0) {
            if($diff < 120) { return 'em um minuto'; }
            if($diff < 3600) { return 'em ' . floor($diff / 60) . ' minutos'; }
            if($diff < 7200) { return 'em uma hora'; }
            if($diff < 86400) { return 'em ' . floor($diff / 3600) . ' horas'; }
        }
        if($day_diff == 1) { return 'amanhã'; }
        if($day_diff < 4) { return date('l', $ts); }
        if($day_diff < 7 + (7 - date('w'))) { return 'próxima semana'; }
        if(ceil($day_diff / 7) < 4) { return 'in ' . ceil($day_diff / 7) . ' semanas'; }
        if(date('n', $ts) == date('n') + 1) { return 'próximo mês'; }
        return date('F Y', $ts);
    }
}


if ( post_password_required() ) {
	return;
}

$error = "";

if (isset($_COOKIE["logged_in"])) {
	$comment_author_email = $_COOKIE["user_email"];
	$comment_author_setor = $_COOKIE["user_setor"];
	$comment_author       = $_COOKIE["user_nome"];
	$comment_author_photourl = $_COOKIE["user_photourl"];
}

if (count($_POST) > 0) {
	$comment_content      = $_POST['comment_content'];
	

	//validate presence of name and message
	if (empty($comment_content)) {
		$error = "Mensagem em branco";
	}
	else //ready to go!
	{

		$commentdata = array(
			'comment_post_ID' => $post->ID,
			'comment_author' => $comment_author,
			'comment_author_url' => '',
			'comment_type' => '', 
			'comment_author_email' => $comment_author_email, 
			'comment_content' => $comment_content
		);

		//Insert new comment and get the comment ID
		$comment_id = wp_new_comment( $commentdata );

		add_comment_meta( $comment_id, 'setor', $comment_author_setor );
		add_comment_meta( $comment_id, 'photourl', $comment_author_photourl );

	}

}

?>
<style>
.form-comment {

}
/*
.comment-item p {
  display: inline-block;
}
*/
.comment-item {
	margin-top: 1em;
}
.comment-item .user_details {
	text-align: center;
	display: inline-block;
	width: 20%;
}
.comment-item .comment_details {
	display: inline-block;
	width: 80%;
	vertical-align: top;
}

.comment-item .user_details p,
.comment-item .comment_details p {
	margin: 0;
}

.comment-item p.avatar_img img{
  width: 100px;
}

.profile-tag{overflow:hidden;display:inline-block;border-radius:10em;position:relative;border:.25em solid #fff;line-height:1em;width:80px; height: 80px;}
.profile-tag span{position:absolute;width:100%;text-align:center;bottom:0;text-transform:uppercase;padding:.1em 0 .4em;display:block;color:#fff}
.profile-tag.gov{border-color:#715d91;}
.profile-tag.gov span{background-color:#715d91}
.profile-tag.edu{border-color:#d47e90;}
.profile-tag.edu span{background-color:#d47e90}
.profile-tag.br{border-color:#bfcf02;}
.profile-tag.br span{background-color:#bfcf02}
.profile-tag.ong{border-color:#6facb1;}
.profile-tag.ong span{background-color:#6facb1}
.profile-tag.com{border-color:#d79c56;}
.profile-tag.com span{background-color:#d79c56}

</style>
<div id="comments" class="comments-area">

	<?php // You can start editing here -- including this comment! ?>

	<?php if ( have_comments() ) : ?>
		<h2 class="comments-title">
			<?php
				printf( _nx( 'Um comentário em &ldquo;%2$s&rdquo;', '%1$s comentário em &ldquo;%2$s&rdquo;', get_comments_number(), 'comments title', 'plataforma-brasil' ),
					number_format_i18n( get_comments_number() ), '<span>' . get_the_title() . '</span>' );
			?>
		</h2>

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through ?>
		<nav id="comment-nav-above" class="comment-navigation" role="navigation">
			<h1 class="screen-reader-text"><?php _e( 'Comment navigation', 'plataforma-brasil' ); ?></h1>
			<div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'plataforma-brasil' ) ); ?></div>
			<div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'plataforma-brasil' ) ); ?></div>
		</nav><!-- #comment-nav-above -->
		<?php endif; // check for comment navigation ?>

		<div class="comment-list">
			<?php
				// wp_list_comments( array(
				// 	'style'      => 'ol',
				// 	'short_ping' => true,
				// ) );

			$comments = get_comments(array(
				'status' => 'approve' //Change this to the type of comments to be displayed
			));
			foreach ($comments as $key => $comment) {
				echo '<div class="comment-item">';
					echo '<div class="user_details">';
echo '<p class="profile-tag '.get_comment_meta($comment->comment_ID, 'setor')[0].' profile-size-80">
<img src="'.get_comment_meta($comment->comment_ID, 'photourl')[0].'" width="80" class="pure-img svg"><span class="profile-desc">'.get_comment_meta($comment->comment_ID, 'setor')[0].'</span></p>';
						echo '<p class="autor">'.$comment->comment_author.'</p>';
					echo '</div>';
					echo '<div class="comment_details">';
						echo '<p class="date"><em>'.time2str($comment->comment_date).'</em></p>';
						echo '<p class="content">'.$comment->comment_content.'</p>';
					echo '</div>';	
				echo '</div>';
				
			}
			?>
		</div><!-- .comment-list -->
<hr />
		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through ?>
		<nav id="comment-nav-below" class="comment-navigation" role="navigation">
			<h1 class="screen-reader-text"><?php _e( 'Comment navigation', 'plataforma-brasil' ); ?></h1>
			<div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'plataforma-brasil' ) ); ?></div>
			<div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'plataforma-brasil' ) ); ?></div>
		</nav><!-- #comment-nav-below -->
		<?php endif; // check for comment navigation ?>

	<?php endif; // have_comments() ?>

	<?php
		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() && '0' != get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
	?>
		<p class="no-comments"><?php _e( 'Comments are closed.', 'plataforma-brasil' ); ?></p>
	<?php endif; ?>

	<?php
	if (isset($_COOKIE["logged_in"]))
	{
		echo '<div class="form-comment">';
		echo '<p class="">Logado como: '.$comment_author.' | <a href="'.WP_PLATBR_URL.'?/sair">Sair</a></p>';
		echo '<form action="'. get_the_permalink() . '" method="post">';
		echo '<p><label for="comment_content">Comentário: <span>*</span> <br><textarea style="width:600px;" rows="6" cols="15" type="text" name="comment_content"></textarea></label></p>';
		echo '<input type="hidden" name="submitted" value="1">';
		echo '<p><input type="submit"></p>';
		echo '</form>';
		echo '</div>';
	} else 
	{
		echo '<a href="'.WP_PLATBR_URL.'?/entrar?back_to='.get_permalink().'">Faça login na plataforma para comentar.</a>';
	}
	?>

	<?php //comment_form(); ?>

</div><!-- #comments -->
