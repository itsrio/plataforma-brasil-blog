<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package plataforma-brasil
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="stylesheet"  href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,400italic,700italic|Fjalla+One">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
<link rel="icon" href="<?php bloginfo('template_directory'); ?>/imgs/favicon.png" type="image/png" />
<?php wp_head(); ?>
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/plataforma-brasil.css">
</head>

<body <?php body_class(); ?>>
<div class="container"><!-- bootstrap container -->
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'plataforma-brasil' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
  <div class="row">
  <div class="col-md-2 col-md-offset-10 social-links">
    <a href="https://www.facebook.com/pages/Plataforma-Brasil/762865640427489" target="_blank"><i class="fa fa-facebook-square"></i></a>
    <!--<a href="http://twitter.com/"><i class="fa fa-twitter-square"></i></a>-->
  </div>
  </div>
  <div class="row">
  <div class="col-md-4">
		<div class="site-branding">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" class="logo" style="text-decoration: none;"><img src="<?php bloginfo('template_directory'); ?>/imgs/logo.svg" alt="Plataforma Brasil">/ BLOG</a>	
</div><!-- .site-branding -->
  </div>
  <!--
  <div class="col-md-4 tagline">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
  </div>
  <div class="col-md-1 visible-md-block visible-lg-block">
    <div class="separator"></div>
  </div>
  <div class="col-md-2">
    <a href="/sobre-nos/" class="learn-more">saiba mais</a>
  </div>
  -->
  </div>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
