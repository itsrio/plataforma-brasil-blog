<?php
/**
 * Jetpack Compatibility File
 * See: http://jetpack.me/
 *
 * @package plataforma-brasil
 */

/**
 * Add theme support for Infinite Scroll.
 * See: http://jetpack.me/support/infinite-scroll/
 */
function plataforma_brasil_jetpack_setup() {
	add_theme_support( 'infinite-scroll', array(
		'container' => 'main',
		'footer'    => 'page',
	) );
}
add_action( 'after_setup_theme', 'plataforma_brasil_jetpack_setup' );
