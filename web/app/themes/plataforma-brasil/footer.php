<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package plataforma-brasil
 */
?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
    <div class="row">
      <div class="col-md-3">
        <h1>Fique informado</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-md-3 newsletter">
        <form action="http://nucleodigital.createsend.com/t/t/s/hyhtg/" method="post">
          <input type="text" name="cm-name" placeholder="Nome">
          <input type="email" name="cm-hyhtg-hyhtg" placeholder="Email">
          <button type="submit" class="pb-button">Enviar</button>
        </form>
      </div>
      <div class="col-md-3 notice">
        Deixe seu nome e e-mail para receber novidades
      </div>
      <div class="col-md-3 col-md-offset-3 copyleft">
      <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licença Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />Este trabalho está licenciado com uma Licença <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons - Atribuição-NãoComercial-CompartilhaIgual 4.0 Internacional</a>.
      </div>
    </div>
	</footer><!-- #colophon -->
</div><!-- #page -->

</div><!-- bootstrap container -->

<?php wp_footer(); ?>
<!-- Facebook Conversion Code for Blog plataforma -->
<script>(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6026427824085', {'value':'0.00','currency':'BRL'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6026427824085&amp;cd[value]=0.00&amp;cd[currency]=BRL&amp;noscript=1" /></noscript>

<!-- Facebook Conversion Code for Blog plataforma -->
<script>(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6027516703788', {'value':'0.00','currency':'BRL'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6027516703788&amp;cd[value]=0.00&amp;cd[currency]=BRL&amp;noscript=1" /></noscript>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-59375443-1', 'auto');
  ga('send', 'pageview');

</script>

</body>
</html>
